import json
import os

from flask import Flask, render_template

from . import util


class default_config(object):
    def __init__(self, app):
        self.DATABASE = os.path.join(app.instance_path, 'video_metadata.sqlite')
        self.MOVIE_DIR = os.path.join(app.instance_path, 'movie_dir')
        self.TEMPLATES_AUTO_RELOAD = True


def create_app():
    # create the app
    app = Flask(__name__, instance_relative_config=True)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # load default config
    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True
    app.config.from_object(default_config(app))

    # add local config changes
    app.config.from_pyfile('local_config.py')

    # TEMPLATES_AUTO_RELOAD wasn't working with uwsgi
    #   https://github.com/pallets/flask/issues/1907#issuecomment-471320980
    if app.config['DEBUG'] == True:
        app.jinja_env.auto_reload = True

    app.logger = util.getLogger(app.config['DEBUG'])

    from . import db
    db.init_app(app)

    from video_metadata.views import file_listing
    app.register_blueprint(file_listing.bp)

    from video_metadata.views import tmdb
    app.register_blueprint(tmdb.bp)

    app.add_url_rule('/', 'index', index)

    return app


def index():
    return render_template('index.html')

