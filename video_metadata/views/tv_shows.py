
import re

from flask import current_app

from .. import db, util


def doTVQuery(query_str:str) -> dict:
    errors = []
    shows = []

    if isTVShow(query_str):
        current_app.logger.debug('looks like TV to me')
        # query is in local cache?
            # get cached list
        # else
            # do tmdb query for titles
    else:
        err = "doesn'tlook like TV to me"
        current_app.logger.debug(err)
        errors.append(err)

    return {'errors': errors, 'shows': shows}


def addTVSeason(tv_id, season_num):
    pass


def addTVEpisode(tv_id, season_num, episode_num):
    pass


def isTVShow(query_str:str) -> bool:
    s = query_str.replace('.', ' ').strip()
    return (None != re.compile(r's[0-9]{1,2}|season ?[0-9]{1,2}', re.IGNORECASE).search(s))
