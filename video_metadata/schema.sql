
DROP TABLE IF EXISTS query_cache;
CREATE TABLE query_cache(
	query_id integer primary key autoincrement,
	query text,
	year integer
);

DROP TABLE IF EXISTS query_details;
CREATE TABLE query_details(
	detail_id integer primary key,
	movie_id integer, -- NOTE: only one of movie_id, tv_id should be set
	tv_id integer,
	media_type integer, -- NOTE: 0=UNKNOWN, 1=MOVIE, 2=TV_SHOW
	title text,
	overview text,
	release_date date,
	original_language text,
	poster_path text
);

DROP TABLE IF EXISTS query_mapping;
CREATE TABLE query_mapping(
	query_id integer not null,
	detail_id integer not null,
	foreign key(query_id) references query_cache(query_id),
	foreign key(detail_id) references query_details(detail_id)
);

DROP TABLE IF EXISTS path_cache;
CREATE TABLE path_cache(
	path text unique not null,
	detail_id integer,
	add_date datetime, -- NOTE: date the path was mapped
	foreign key(detail_id) references query_details(detail_id)
);

