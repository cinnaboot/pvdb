
# NOTE: copy this file to the instance folder and fill in the appropriate values

# https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY
SECRET_KEY = b'\x00'

# https://flask.palletsprojects.com/en/1.1.x/config/#DEBUG
DEBUG = False

# the absolute filesystem path to use as your video directory eg) '/var/videos'
MOVIE_DIR = ''

# create an account at tmdb and follow the instructions to find your key
#   https://developers.themoviedb.org/3/getting-started/introduction
TMDB_API_KEY = ''

# location of your video files relative to your web server's root eg) '/video'
VIDEO_URL_PREFIX = ''

