#!/bin/sh

export FLASK_APP=video_metadata
export FLASK_ENV=development


show_help() {
	echo "Usage: $0 [OPTION]"
	echo "Without options, run the flask builtin server."
	echo ""
	echo "-h, --help      display this help and exit"
	echo "    --initdb    (re-)initialize the the local sqlite DB from schema"
	echo "    --shell     enter interactive python session with flask context"
	echo ""
}

case $1 in
	-h|--help)
		show_help
		;;
	--initdb)
		flask init-db
		;;
	--shell)
		flask shell
		;;
	"")
		flask run
		;;
	*)
		show_help
		;;
esac

